using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nancy.Json;

namespace OKLottery
{
    class OKLottery
    {
        //Create the object
        public class GamePlay
        {
            public DateTime date_played { get; set; }
            public string[] games_played { get; set; }
        }
        static void Main(string[] args)
        {
            //Serialize the Json file to a list
            string inputText = "";
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("https://www.lottery.ok.gov/plays.json");
                inputText = json;
            }            
            JavaScriptSerializer ser = new JavaScriptSerializer();
            var GameList = ser.Deserialize<List<GamePlay>>(inputText);

            //Find All unique names in the file
            List<string> AllGames = new List<string>();

            foreach (GamePlay play in GameList)
            {
                for (int i = 0; i < play.games_played.Length; i++)
                {
                    if (!AllGames.Contains(play.games_played[i]))
                    {
                        AllGames.Add(play.games_played[i]);
                    }
                }
            }

            //Check to see if each pair of games have been played together, count and output to console
            for (int i = 0; i + 1 < AllGames.Count; i++)
            {
                for(int j = i + 1; j < AllGames.Count; j++)
                {
                    int count = 0;
                    foreach(GamePlay play in GameList)
                    {
                        if (play.games_played.Contains(AllGames[i]) && play.games_played.Contains(AllGames[j]))
                        { 
                            count++;
                        }                        
                    }
                    string output = AllGames[i] + " and " + AllGames[j] + " have been played together " + count.ToString() + " times.";
                    Console.WriteLine(output);
                }
            }
        }
    }
}
